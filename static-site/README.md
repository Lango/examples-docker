// Install (needs to be run first)
```
docker run \
  --rm \
  -i \
  -t \
  -v $(pwd):/app \
  jmfirth/webpack \
  npm install --no-bin-links
```


// Build
```
docker run \
  --rm \
  -i \
  -t \
  -v $(pwd):/app \
  jmfirth/webpack \
  webpack
```

// Run
```
docker run \
  --rm \
  -i \
  -t \
  -v $(pwd):/app \
  -p 3000:8080 \
  jmfirth/webpack \
  webpack-dev-server --hot --inline --progress --host 0.0.0.0
```  