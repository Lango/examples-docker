From http://www.dwmkerr.com/learn-docker-by-building-a-microservice/?utm_source=hackernewsletter&utm_medium=email&utm_term=fav

Commands:

Start mysql:
`docker run --name db -d -e MYSQL_ROOT_PASSWORD=123 -p 3306:3306 mysql:latest`

Build a new image for users-service
```
# Builds a new image (in ./users-service)
$ docker build -t users-service .

# Run a container with this image, interactive
$ docker run -it -p 8123:8123 users-service
```
Link docker containers
`docker run -it -p 8123:8123 --link db:db -e DATABASE_HOST=DB users-service`

Docker-compose
```
docker-compose build
docker-compose up
docker-compose down
```