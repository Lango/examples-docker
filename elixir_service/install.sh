#!/usr/bin/env bash
set -e

echo '>>> Get old container id'
CID=$(docker ps -a | grep "examples-docker" | awk '{print $1}')
echo $CID

echo '>>> Get latest image'
docker pull registry.gitlab.com/lango/examples-docker:latest

echo '>>> Stopping old container'
if [ "$CID" != "" ];
then
  docker stop $CID
fi

echo '>>> Delete old container'
if [ "$CID" != "" ];
then
  docker rm $CID
fi

echo '>>> Remove old docker image'
docker images | grep "<none>" | head -n 1 | awk 'BEGIN { FS = "[ \t]+" } { print $3 }'  | while read -r id ; do
   docker rmi $id
done

echo '>>> Starting new container'
docker run -d --name examples-docker -p 4000:4000 registry.gitlab.com/lango/examples-docker:latest

echo '>>> Sleep for 30 seconds to give time for server to start up'
sleep 30

echo '>>> ALL DONE :)'