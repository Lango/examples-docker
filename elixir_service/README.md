# Docker stuffBuild a new image for users-service

```
# Builds a new image (in ./users-service)
$ docker build -t elixir_service .

# Run a container with this image, interactive
$ docker run -it --name examples-docker -p 4000:4000 elixir_service

or

$ docker run -it --name examples-docker -p 4000:4000 registry.gitlab.com/lango/examples-docker:latest
```

To use the image from gitlab
```
$ docker run -it -p 4000:4000 registry.gitlab.com/lango/examples-docker:latest
```

https://coderwall.com/p/ag1agw/automated-update-a-running-docker-container-and-cleanup-old-version-of-it

To get latest docker image
```
$ docker pull registry.gitlab.com/lango/examples-docker:latest
```

To stop docker container
```
$ docker stop examples-docker
```

To delete docker  container
```
$ docker rm examples-docker
```

To remove docker images (because they won't have a tag)
```
$ docker images | grep "^<none>" | head -n 1 | awk 'BEGIN { FS = "[ \t]+" } { print $3 }'  | while read -r id ; do
   docker rmi $id
done
```

To run in the background (remember it takes a while to start up)
```
$ docker run -d --name examples-docker -p 4000:4000 registry.gitlab.com/lango/examples-docker:latest
```

# Deploying to gcloud

1. Install the gcloud tools
2. Tag with gcr.io/[project_id]/examples-docker:[tag]
3. Push the gcr.io... image (this will go to google cloud)
4. Create cluster (if you haven't already)
5. `gcloud container clusters get-credentials examples-docker`
6. Create pod (if you want multiple containers)
6.1 `kubectl run examples-docker --image=gcr.io/examples-docker/examples-docker:latest --port=4000`
7. Allow external traffic? `kubectl expose deployment examples-docker --type="LoadBalancer" --port=80 --target-port=4000`
7. Get the ip `kubectl get services examples-docker`
8. How does it auto update?

Note: If too expensive try running multiple containers on a digitalocean

# Details

Run any command like so:

`docker run -it --rm -v $(pwd):/code/elixir_service -w /phoenix marcelocg/phoenix [command]`

To Setup (--no-ecto removes database stuff for now)
(I think this needs -w set for it to persist)
`docker run -it --rm -v $(pwd):/code/elixir_service -w /phoenix marcelocg/phoenix mix phoenix.new /code/elixir_service --no-ecto`

To install dependencies
`docker run -it --rm -v $(pwd):/code/elixir_service -w /code/elixir_service marcelocg/phoenix mix deps.get`

To install static npm stuff
`docker run -it --rm -v $(pwd):/code/elixir_service -w /code/elixir_service marcelocg/phoenix npm install`

To build static stuff
`docker run -it --rm -v $(pwd):/code/elixir_service -w /code/elixir_service marcelocg/phoenix npm install && node node_modules/brunch/bin/brunch build`

You also need to install inotify-tools at some point

Database (we are ignoring this for now)
`docker run -it --rm -v $(pwd):/code/elixir_service -w /code/elixir_service marcelocg/phoenix mix ecto.create`

To start (had to run this twice before)
`docker run -it --rm -v $(pwd):/code/elixir_service -p 4000:4000 -w /code/elixir_service marcelocg/phoenix mix phoenix.server`

mrrooijen/phoenix is a good page for docs

# ElixirService

To start your Phoenix app:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Install Node.js dependencies with `npm install`
  * Start Phoenix endpoint with `mix phoenix.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](http://www.phoenixframework.org/docs/deployment).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
