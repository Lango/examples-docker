defmodule ElixirService.PageControllerTest do
  use ElixirService.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "David's Phoenix Masterpiece!"
  end
end
